# Semiconductor Devices

![Logo](https://gitlab.com/armos-ud/devices/-/blob/main/logo.png "ARMOS")

## Course on Semiconductor Devices prepared by the ARMOS research group.

A **semiconductor device** is an electronic component whose operation depends on the electronic properties of a semiconductor material. Its conductivity lies between that of conductors and that of insulators. Basic semiconductor devices include the diode and the transistor.. The training material and code is developed in Python and Jupyter Notebook.
