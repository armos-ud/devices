{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction to Ahkab"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this section we'll go through some preliminary topics, as well as some of the\n",
    "requirements for this tutorial.\n",
    "\n",
    "By the end of this section you should:\n",
    "\n",
    "- Know what sort of tasks qualify as Machine Learning problems.\n",
    "- See some simple examples of machine learning\n",
    "- Know the basics of creating and manipulating numpy arrays.\n",
    "- Know the basics of scatter plots in matplotlib."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What is Ahkab?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ahkab is a SPICE-like electronic circuit simulator written in Python (2 or 3), and it is **platform-independent.** It supports numerical and symbolic simulations.\n",
    "\n",
    "- Numeric:\n",
    " - Operating point, with guess computation to speed up the solution.\n",
    " - DC sweep\n",
    " - Transient analysis, available differentiation formulas: implicit Euler, trapezoidal, gear orders from 2 to 5.\n",
    " - AC analysis.\n",
    " - Periodic steady state analysis of non-autonomous circuits, time domain shooting and brute-force algorithms.\n",
    " - Pole-zero analysis.\n",
    "\n",
    "- Symbolic:\n",
    " - Small signal analysis, AC or DC, with extraction of transfer functions, DC gain, poles and zeros."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Why Ahkab?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Ahkab circuit simulator is an experiment. There is no expectation that this small circuit simulation tool will be replacing the mainstream circuit simulators: they are mainstream for good reasons. What we wish to do with Ahkab is to allow the user, the designer, to peek behind the veil and see more clearly what goes on with his simulations. For this reason, Ahkab supports operations such as printing out all equation matrices.\n",
    "\n",
    "It is also written in a scripted, interpreted language (Python) that, while requiring us to sacrifice raw speed, should make it relatively easy to see what’s going on behind the hood. And the algorithms are there for you to see, inspect and, if need be, correct: too often scientific papers about software come with no available implementation or the source is not distributed: with Ahkab all code is available under a copy-left license, allowing you to benefit of the code, modify it and giving others the same freedom.\n",
    "\n",
    "With Ahkab, you are welcome to implement an algorithm you have read about in a paper, if you are so inclined: we believe no lecture, no matter how in-depth, will provide an insight in a circuit simulation algorithm as deep as rolling up your sleeves and trying to code it up yourself."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Command line help"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Ahkab simulator has a command line interface that allows for quick simulation of netlist decks, without the need to load the Python interpreter explicitely. Several switches are available, to set the input and output files and to override some built-in options.\n",
    "\n",
    "Usage: **ahkab [options] <filename>**\n",
    "\n",
    "The filename is the netlist to be open. However, this is the command line interface, we are using **Ipython Notebook**, for it to work you must add the symbol **!**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "!ahkab --help"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The netlist file"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Circuits are described in text files called netlists. Each line in a netlist file falls in one of these categories:\n",
    "- The title.\n",
    "- A element declaration.\n",
    "- A analysis declaration.\n",
    "- A directive declaration (e.g. .ic or .end).\n",
    "- A comment. Comments start with *.\n",
    "- A continuation line. Continuation lines start with +.\n",
    "- Blank line (ignored)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Title"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The title is a special type of comment and it is always the first line in the file. Do not put any other directive here, it will be silently ignored.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Elements"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In general, an element is declared with the following general syntax:\n",
    "\n",
    "**[K][description_string] [n1] [n2] [value] [option=value] [...]**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Where:\n",
    "- K is a character, a unique identifier for each type of element (e.g. R for resistor).\n",
    "- description_string is a string without spaces (e.g. 1).\n",
    "- n1, a string, is the node of the circuit to which the anode of the element is connected.\n",
    "- n2, a string, is the node of the circuit to which the cathode of the element is connected.\n",
    "- [value] if supported, is the ‘value’ of the element, in mks (e.g. R1 1 0 500k)\n",
    "- option=value are the parameters of the element.\n",
    "\n",
    "Nodes may have any label, without spaces, except the reference node (GND) which has to be 0."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### - Resistor"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**R[string] n1 n2 [value]**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "n1 and n2 are the element nodes, and value is the element resistance. It may any non-zero value (negative values are supported too)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**R1 1 0 1k**\n",
    "\n",
    "**RAb_ input output 1.2e6**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### - Capacitor"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**C[string] n1 n2 [value] [ic=value2]**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "n1 and n2 are the element nodes, value is the capacitance in Farads, and ic=value2 is an optional attribute that can be set to provide an initial voltage value for a transient simulation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**C1 1 0 1u**\n",
    "\n",
    "**Cfeedback out+ in- 1e6**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### - Inductor"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**L[string] n1 n2 [value] [ic=value2]**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "n1 and n2 are the element nodes, value is the inductance in Henry, and ic=value2 is an optional attribute that can be set to provide an initial value for a transient simulation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**L1 1 0 1u**\n",
    "\n",
    "**Lchoke inA inB 1e6**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### - Mutual inductors"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**K[string] inductor1 inductor2 [value]**\n",
    "\n",
    "or\n",
    "\n",
    "**K[string] inductor1 inductor2 k=[value]**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "inductor1 and inductor2 are the coupled inductors. They need to be specified before the coupling can be inserted. value is the coupling factor, k. It is a needs to be less than 1."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**L1 1 0 1u**\n",
    "\n",
    "**L2 3 4 5u**\n",
    "\n",
    "**K1 L1 L2 0.6**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### - Voltage source"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**v[string] n1 n2 [type=vdc vdc=float] [type=vac vac=float] [type=....]**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Where the third type (if added) is one of: sin, pulse, exp, sffm, am."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Time functions may be used in conjunction with an independent source to define its time-dependent behavior. This is typically done adding a **type=...** section in the element declaration, such as:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**V1 1 2 vdc=10m type=sin VO=10m VA=1.2 FREQ=500k TD=1n THETA=0**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "with the syntax:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**type=sin VO=float VA=float FREQ=float TD=float THETA=float PHASE=float**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Mathematically:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from IPython.display import display, Math, Latex\n",
    "display(Math('t < TD: {\\quad} V (t) = VO'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from IPython.display import display, Math, Latex\n",
    "display(Math('t {\\geq} TD: {\\quad} V (t) = VO + VA \\cdot e^{-THETA\\cdot(t-TD)}\\cdot sin(2\\pi FREQ(t-TD)+ {PHASE}/{360})'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- VO is the offset voltage in Volt.\n",
    "- VA is the amplitude in Volt.\n",
    "- FREQ is the frequency in Hertz.\n",
    "- TD is the delay in seconds.\n",
    "- THETA is the damping factor per second.\n",
    "- PHASE is the phase in degrees."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### - Current source"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**i[string] n1 n2 [type=idc idc=float] [type=iac iac=float] [type=....]**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The declaration of the time variant part is the same as for voltage sources, except that vo becomes io, va becomes ia and so on."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### - Diode"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**D[string] n1 n2 [model_id] [AREA=float T=float IC=float OFF=boolean]**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- n1: anode.\n",
    "- n2: cathode.\n",
    "- model_id: the ID of the diode model.\n",
    "- AREA: The area of the PN junction.\n",
    "- T: the temperature of operation, if different from the circuit temperature.\n",
    "- IC: initial condition statement (voltage).\n",
    "- OFF: Consider the diode to be initially off in transient analyses."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Analyses"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### - Operating point (.OP)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**.op [guess=ic_label]**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This analysis tries to find a DC solution through a pseudo Newton Rhapson (NR) iteration method. Notice that a non-linear circuit may have zero, a discrete number or infinite OPs. guess is an initial guess to speed up convergence. The t = 0 value is automatically added as DC value to every time-variant independent source without a explicit DC value."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### - DC analysis (.DC)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**.DC src=src_name start=float stop=float step=float type=lin/log**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Performs a DC sweep (repeated OP analysis with the value of a voltage or current source changing at every iteration).\n",
    "- src: the id of the source to be swept (V12, Ibias...). Only independent current and voltage sources.\n",
    "- start and stop: sweep start and stop values.\n",
    "- step: sets the value of the source from an iteration (k) to the next (k + 1):\n",
    "- type: either lin or log"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### - Transient analysis (.TRAN)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**.TRAN TSTEP=float TSTOP=float [TSTART=float UIC=0/1/2/3 [IC_LABEL=string] METHOD=string]**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Performs a transient analysis from TSTART (which defaults to 0) to TSTOP, using the step provided as initial step and the method specified (if any, otherwise defaults to implicit Euler).\n",
    "- TSTART: the starting point, defaults to zero.\n",
    "- TSTEP: this is the initial step. By default, the program will try to adjust it to keep the estimate error within bounds.\n",
    "- TSTOP: Stop time.\n",
    "- UIC (Use Initial Conditions): This is used to specify the state of the circuit at time t = TSTART. Available values are 0, 1, 2 or 3.\n",
    "- UIC=0: all node voltages and currents through v/h/e/sources will be assumed to be zero at t = TSTART.\n",
    "- UIC=1: the status at t = TSTART is the last result from a OP analysis.\n",
    "- UIC=2: the status at t = TSTART is the last result from a OP analysis on which are set the values of currents through inductors and voltages on capacitors specified in their ic. This is done very roughly, checking is recommended.\n",
    "- UIC=3: Load a user supplied ic. This requires a .ic directive somewhere in the netlist and a .ic name and ic_label must match.\n",
    "- METHOD: the integration method to be used in transient analysis. Built-in methods are: implicit_euler, trap, gear2, gear3, gear4, gear5 and gear6. Defaults to trap."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### - AC analysis (.AC)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**.AC start=float stop=float nsteps=integer sweep_type=lin/log**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Performs an AC analysis. If the circuit is non-linear, a successful Operating Point (OP) is needed to linearize the circuit.\n",
    "- start: the starting frequency of the sweep, in Hz.\n",
    "- stop: the final angular frequency, in Hz.\n",
    "- nsteps: the number of steps to be executed.\n",
    "- sweep_type: a parameter that can be set to LOG or LIN (the default), selecting a logarithmic or a linear frequency sweep."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "###  Directive declarations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### .Plot"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**.plot [simulation_type] [variable1 variable2 ... ]**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plot the results from simulation.\n",
    "- simulation_type: which simulation will have the data plotted. Currently the available options are tran, pss, ac and dc.\n",
    "- variable1, variable2: the signals to be plotted. A voltage syntax V(<node>), to plot the voltage at the specified node, or V(<node2>, <node1>), to plot the difference of the node voltages. E.g. V(in) or V(2,1).\n",
    "\n",
    "Plotting is possible only if matplotlib is available."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### .Four"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**.FOUR [freq] var1 [var2 var3 ...]**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Perform a Fourier analysis over the latest transient data.\n",
    "- freq: the fundamental frequency, in Hz.\n",
    "- var1, var2 ... : the signals to execute the FOUR analysis on. Each signal is treated independently.\n",
    "\n",
    "The Fourier analysis is performed over the interval which is decided as follows:\n",
    "- The data should be taken from the end of the simulation, so that if there is any build-up or stabilization process, the Fourier analysis is not affected (or less affected) by it.\n",
    "- At least 1 period of the fundamental has to be used.\n",
    "- Not more than 50% of the total simulation time should be used, if possible.\n",
    "- Respecting the above, as much data as possible should be used, as it leads to more accurate results."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### .end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Force the parser to stop reading the netlist. Everything after this line is disregarded."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### .Ic"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**.ic name=ic_label [v(node)=value i(element_name)=value ... ]**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Set an Initial Condition for circuit analysis. This allows the specification of a state of a circuit. Every node voltage or current (through appropriate elements) may be specified. If not set, it will be set to 0. Notice that setting an inappropriate or inconsistent IC will create convergence problems."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Matplotlib"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The most common tool for visualization of data in Python is `matplotlib`.  It is an extremely flexible package, but\n",
    "we will go over some basics here.\n",
    "\n",
    "First, something special to IPython notebook.  We can turn on the \"IPython inline\" mode,\n",
    "which will make plots show up inline in the notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "%pylab inline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# plotting a line\n",
    "\n",
    "x = np.linspace(0, 10, 100)\n",
    "plt.plot(x, np.sin(x))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# scatter-plot points\n",
    "\n",
    "x = np.random.normal(size=500)\n",
    "y = np.random.normal(size=500)\n",
    "plt.scatter(x, y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# showing images\n",
    "x = np.linspace(1, 12, 100)\n",
    "y = x[:, np.newaxis]\n",
    "\n",
    "im = y * np.sin(x) * np.cos(y)\n",
    "print im.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# imshow - note that origin is at the top-left!\n",
    "plt.imshow(im)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Contour plot - note that origin here is at the bottom-left!\n",
    "plt.contour(im)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## OP Example"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Performing numeric Operating Point (OP) simulations with Ahkab of linear networks is really straight-forward. The first step is to draw the circuit, with labels for elements and nodes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Design the circuit\n",
    "from IPython.display import Image\n",
    "Image(filename='img/lineal.png')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First of all, we need the Ahkab library to be imported:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import ahkab\n",
    "from ahkab import circuit, printing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we create a new **circuit object**. The only parameter you need is the name, which in the following is ‘Simple Example Circuit’."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "mycir = ahkab.Circuit('Simple Example Circuit')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now’s time to add the elements. The circuit object we just created (**mycir**) offers plenty of convenience methods to add elements to your circuit instances. The general structure of the methods signature is:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "mycir.add_resistor('R1', 'n1', mycir.gnd, value=5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This adds resistance **R1** to the object. Similarly we add the other elements of the circuit."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "mycir.add_vsource('V1', 'n2', 'n1', dc_value=8)\n",
    "mycir.add_resistor('R2', 'n2', mycir.gnd, value=2)\n",
    "mycir.add_vsource('V2', 'n3', 'n2', dc_value=4)\n",
    "mycir.add_resistor('R3', 'n3', mycir.gnd, value=4)\n",
    "mycir.add_resistor('R4', 'n3', 'n4', value=1)\n",
    "mycir.add_vsource('V3', 'n4', mycir.gnd, dc_value=10)\n",
    "mycir.add_resistor('R5', 'n2', 'n4', value=4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we need the OP simulation object. And then we start the simulation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "opa = ahkab.new_op()\n",
    "result = ahkab.run(mycir, opa)['op']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Results. Simply printing the results with **print** formats the simulation results in a nice table:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print result"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## TRANS Example"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let’s say we would like to simulate the transient response of an RLC circuit."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Design the circuit\n",
    "from IPython.display import Image\n",
    "Image(filename='img/filtro2.png')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Import Ahkab library (and others!) and create a new circuit object\n",
    "import ahkab\n",
    "from ahkab import circuit, printing, time_functions\n",
    "import math\n",
    "import numpy as np\n",
    "import pylab\n",
    "\n",
    "mycir2 = ahkab.Circuit('Simple RLC Example Circuit')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Elements are to be connected to nodes. There is one special node, the reference (gnd):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "gnd = mycir2.get_ground_node()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Add the elements\n",
    "mycir2.add_resistor('R1', 'n1', 'n2', value=600)\n",
    "mycir2.add_inductor('L1', 'n2', 'n3', value=15.00e-3)\n",
    "mycir2.add_capacitor('C1', 'n3', gnd, value=120.00e-9)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For the voltage source V1, we define a sine function to provide the time-variable characteristics of V1, to be used in\n",
    "the transient simulation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "sin60 = time_functions.sin(vo=0, va=10, freq=60)\n",
    "mycir2.add_vsource('V1', 'n1', gnd, 1, function=sin60)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now check that the circuit is defined as we intended, generating a netlist."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print mycir2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we need to define the analyses to be carried out:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "tran_analysis = ahkab.new_tran(tstart=0, tstop=35.0e-3, tstep=1e-6, x0=None)\n",
    "result2 = ahkab.run(mycir2, tran_analysis)['tran']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To graph the results, we first import mathplotlib, and construct the time axis."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "t = result2.get_x()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will observe the voltage on the three nodes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "result2['vn1'], result2['vn2'], result2['vn3']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We generate the graph and keep a copy."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "fig = pylab.figure()\n",
    "pylab.hold(True)\n",
    "pylab.plot(t, result2['vn1'], 'o', ms=3, label='V(n1) (Input)')\n",
    "pylab.plot(t, result2['vn3'], 'o', ms=3, label='V(n3) (Output)')\n",
    "pylab.legend()\n",
    "pylab.xlabel('t [s]')\n",
    "pylab.ylabel('Voltage [V]')\n",
    "fig.savefig('img/tran_plot.png')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "Image(filename='img/tran_plot.png')"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
